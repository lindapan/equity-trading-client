import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RunningStrategyComponent } from './running-strategy.component';

describe('RunningStrategyComponent', () => {
  let component: RunningStrategyComponent;
  let fixture: ComponentFixture<RunningStrategyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RunningStrategyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RunningStrategyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
