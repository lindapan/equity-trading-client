import { Component, OnInit } from '@angular/core';
import { StockService } from '../shared/stock/stock.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-stocks',
  templateUrl: './stocks.component.html',
  styleUrls: ['./stocks.component.css']
})
export class StocksComponent implements OnInit {

  stock : String;
  valid : boolean = true;
  stockName : String = '';

  constructor(private stockService: StockService, private router: Router) {}

  ngOnInit() {

    // this.stockService.getAll().subscribe(data => {
    //   this.stock = data;

    //   // console.log(this.stock);
    //   // console.log(this.stock.name);
    // });
  }

  getStock() {
    this.valid = true;
    console.log("input stock name: "+ this.stockName);
    if(!this.stockName) {
      this.valid = false;
      return;
    }
    this.stockService.getStock(this.stockName).subscribe(data => {
      this.stock = data;
      console.log("get stock output"+this.stock);
      console.log(this.valid);
    });



  }


}
