import { Component  } from '@angular/core';
import { StockService } from '../shared/stock/stock.service';
import { ChartsModule } from 'ng2-charts';
import * as Chart from 'chart.js';
//import Rx from "rxjs/Rx";

@Component({
  selector: 'app-graph-page',
  templateUrl: './graph-page.component.html',
  styleUrls: ['./graph-page.component.css']
})
export class GraphPageComponent {
    monitoredStocks : String[] = [];
    stockName : String = '';
    valid : boolean = true;

  constructor(private stockService: StockService){
    //Observable.interval(10000).takeWhile(() => true).subscribe(() => this.function());
  }

  function(){
    console.log("I am being called!");
  }

  // lineChart
  public lineChartData:Array<any> = [
    {data: [65, 59, 80, 81, 56, 55, 40, 100, 43, 56], label: 'Series A'}
  ];
  public lineChartLabels:Array<any> = ['0s','15s','30s','45s','60s','75s','90s','105s','120s','135s','150s','165s','180s','195s','210s','225s','240s','255s','270s','285s','300s','315s','330s','345s','360s','375s','390s','405s','420s','435s','450s','465s','480s','495s','510s','525s','540s','555s','570s','585s','600s','615s','630s','645s','660s','675s','690s','705s','720s','735s','750s','765s','780s','795s','810s','825s','840s','855s','870s','885s','900s','915s','930s','945s','960s','975s','990s','1005s','1020s','1035s','1050s','1065s','1080s','1095s','1110s','1125s','1140s','1155s','1170s','1185s','1200s','1215s','1230s','1245s','1260s','1275s','1290s','1305s','1320s','1335s','1350s','1365s','1380s','1395s','1410s','1425s','1440s','1455s','1470s','1485s','1500s','1515s','1530s','1545s','1560s','1575s','1590s','1605s','1620s','1635s','1650s','1665s','1680s','1695s','1710s','1725s','1740s','1755s','1770s','1785s'];

  public lineChartOptions:any = {
    responsive: true
  };
  public lineChartColors:Array<any> = [
    { // grey
      backgroundColor: 'rgba(148,159,177,0.2)',
      borderColor: 'rgba(255, 99, 132, 1)',
      pointBackgroundColor: 'rgba(255, 99, 132, 1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    { // dark grey
      backgroundColor: 'rgba(77,83,96,0.2)',
      borderColor: 'rgba(31, 218, 34,1)',
      pointBackgroundColor: 'rgba(31, 218, 34,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(77,83,96,1)'
    },
    { // grey
      backgroundColor: 'rgba(148,159,177,0.2)',
      borderColor: 'rgba(148,159,177,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    }
  ];
  public lineChartLegend:boolean = true;
  public lineChartType:string = 'line';

  public updateChart(days, prices, stock):void {
    let _lineChartData:Array<any> = new Array(this.lineChartData.length);
    _lineChartData[0] = {data: new Array(prices.length), label: stock};
    for (let j = 0; j < prices.length; j++) {
      _lineChartData[0].data[j] = prices[j];
    }
    this.lineChartData = _lineChartData;
  }

  public randomize():void {
    let _lineChartData:Array<any> = new Array(this.lineChartData.length);
    for (let i = 0; i < this.lineChartData.length; i++) {
      _lineChartData[i] = {data: new Array(this.lineChartData[i].data.length), label: this.lineChartData[i].label};
      for (let j = 0; j < this.lineChartData[i].data.length; j++) {
        _lineChartData[i].data[j] = Math.floor((Math.random() * 100) + 1);
      }
    }
    this.lineChartData = _lineChartData;
  }

  public update(){
    for(let i = 0; i < this.monitoredStocks.length; i++){
      this.updateData(this.monitoredStocks[i]);
    }
  }

  addStock() {
    this.valid = true;
    console.log("input stock name: "+ this.stockName);
    if(!this.stockName) {
      this.valid = false;
      return;
    }
    this.monitoredStocks.push(this.stockName);
    this.updateData(this.stockName);
    //this.pullNewMarketData();
  }

  updateData(stockName) {
    this.stockService.getStock(stockName).subscribe(data => {
      let stock = data;
      let historical = JSON.parse("[" + data.historicalPrice + "]");
      let time = JSON.parse("[" + data.timePeriod + "]");
      this.updateChart(time, historical, stock.name);
    });
  }
}
