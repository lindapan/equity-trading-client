import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CATCH_ERROR_VAR } from '../../../../node_modules/@angular/compiler/src/output/output_ast';

@Injectable()
export class StockService {

  constructor(private http: HttpClient) {
  }

  // getAll(): Observable<any> {
  //   return this.http.get('//localhost:8081/getStock');
  // }

  getStock(stockName) : Observable<any> {
    return this.http.post('//localhost:9080/getStock', stockName);
  }

  startPricingService() : Observable<any> {
    return this.http.post('//localhost:9080/startPricingService', "asdf");
  }

}
