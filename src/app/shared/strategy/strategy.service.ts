import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StrategyService {


  constructor(private http: HttpClient) { }

  addStrategy(list) : Observable<any> {
    return this.http.post('//localhost:9080/addStrategy', list);

  }


  getRunningStrategy() : Observable<any> {
    return this.http.get('//localhost:9080/getRunningStrategy');

  }

  getPausedStrategy() : Observable<any> {
    return this.http.get('//localhost:9080/getPausedStrategy');

  }

  pauseStrategy(id) : Observable<any> {
    return this.http.post('//localhost:9080/pauseStrategy', id);

  }

  stopStrategy(id) : Observable<any> {
    return this.http.post('//localhost:9080/stopStrategy', id);

  }

  startStrategy(id) : Observable<any> {
    return this.http.post('//localhost:9080/startStrategy', id);

  }

  getPerformance(id) : Observable<any> {
    return this.http.post('//localhost:9080/getPerformance', id);

  }

}
