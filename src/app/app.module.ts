import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { StocksComponent } from './stocks/stocks.component';
import { StockDetailComponent } from './stock-detail/stock-detail.component';

import { StockService } from './shared/stock/stock.service';
import { StrategyService } from './shared/strategy/strategy.service';
import { HttpClientModule }  from '@angular/common/http';
import { RouterModule, Router } from '@angular/router';
import { AppRoutingModule } from './/app-routing.module';

import { StrategyComponent } from './strategy/strategy.component';
import { NavbarComponent } from './navbar/navbar.component';
import { GraphPageComponent } from './graph-page/graph-page.component';
import { RunningStrategyComponent } from './running-strategy/running-strategy.component';
import { ChartsModule } from 'ng2-charts';


@NgModule({
  declarations: [
    AppComponent,
    StocksComponent,
    StockDetailComponent,
    NavbarComponent,
    GraphPageComponent,
    StrategyComponent,
    RunningStrategyComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule,
    AppRoutingModule,
    ChartsModule
  ],
  providers: [StockService, StrategyService],
  bootstrap: [AppComponent]
})
export class AppModule { }
