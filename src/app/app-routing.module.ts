import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GraphPageComponent } from './graph-page/graph-page.component';
import { StrategyComponent } from './strategy/strategy.component';
import { RunningStrategyComponent } from './running-strategy/running-strategy.component';

import { StockDetailComponent } from './stock-detail/stock-detail.component';
import { StocksComponent } from './stocks/stocks.component';

const routes: Routes = [
  { path: '', redirectTo: '/graphs', pathMatch: 'full' },
  { path: 'graphs', component: GraphPageComponent },
  { path: 'runningStrategy', component: RunningStrategyComponent },
  
  { path: 'stockDetail', component: StockDetailComponent },
  { path: 'stocks', component: StocksComponent },
  { path: 'strategy', component: StrategyComponent }
];

@NgModule({
  exports: [ RouterModule ],
  imports: [ RouterModule.forRoot(routes) ]
})
export class AppRoutingModule {}
