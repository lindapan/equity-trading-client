export class Stock {
    name : string;
	highPrice : number;
	lowPrice : number;
	currPrice : number;
	open : number;
	close : number;
    volume : number;
}