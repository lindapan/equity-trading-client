import { Component, OnInit } from '@angular/core';
import { StrategyService } from '../shared/strategy/strategy.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-strategy',
  templateUrl: './strategy.component.html',
  styleUrls: ['./strategy.component.css']
})
export class StrategyComponent implements OnInit {
  id  : String;
  stock : String;
  strategy : String = "";
  stopping : String;
  exit : String;
  amount : String;
  longMA : String;
  shortMA : String;
  windowBB : String;
  deviationBB : String;
  list : Array<any>;
  pnl : String;
  roi : String;
  currentid : String;

  response : String;
  status : boolean = true;
  trigger : boolean = false;
  running : boolean = true;

  runningcolumns = ["id", "stockName", "type", "stoppingThreshold", "exitThreshold", "amount", "param1", "param2", "Pause" ,"Remove", "Performance"];
  pausecolumns = ["id", "stockName", "type", "stoppingThreshold", "exitThreshold", "amount", "param1", "param2", "Start"];
  run : any;
  pause : any;


  constructor(private strategyService: StrategyService, private router: Router) { }

  ngOnInit() {
   this.getRunningStrategy();
   
    
  }

  strategies = ['', 'TwoMA', 'BollingerBands'];

  submitted = false;
  onSubmit() {this.submitted = true;}

  addStrategy(){
    this.trigger = true;
    if  (this.strategy == 'TwoMA') {
      this.list = [this.id, this.strategy, this.stock, this.stopping, this.exit, this.amount, this.longMA, this.shortMA];
      console.log(this.list);
    } else if (this.strategy == "BollingerBands") {
      this.list = [this.id, this.strategy, this.stock, this.stopping, this.exit, this.amount, this.windowBB, this.deviationBB];
      console.log(this.list);
    }


    this.strategyService.addStrategy(this.list).subscribe(data => {
      this.response = data.status;

      console.log(this.response)
      if(this.response == "success") {
        this.status = true;
        console.log("success!!");

      }else if (this.response == "failed") {
        this.status = false;
        console.log("failed!!")
      }

    });

    this.id = "";
    this.stock = "";
    this.strategy = "";
    this.stopping = "";
    this.exit = "";
    this.amount = "";
    this.longMA = "";
    this.shortMA = "";
    this.windowBB = "";
    this.deviationBB = "";
  }

  getRunningStrategy() {
    console.log("running strategy");
    this.strategyService.getRunningStrategy().subscribe(data => {
      console.log(data);
      this.run = data;

    });


    
  }

  getPausedStrategy() {
    console.log("paused strategy");
    this.strategyService.getPausedStrategy().subscribe(data => {
      console.log(data);
      this.pause = data;
    });
  }

  pauseStrategy(id) {
    console.log(id);
    this.strategyService.pauseStrategy(id).subscribe(data => {
      
      // this.pause = data;
      this.getRunningStrategy();
      this.getPausedStrategy();
    });

  }
  
  stopStrategy(id) {
    console.log(id);
    this.strategyService.stopStrategy(id).subscribe(data => {
      
      // this.pause = data;
      this.getRunningStrategy();
      this.getPausedStrategy();
    });

  }

  startStrategy(id) {
    console.log(id);
    this.strategyService.startStrategy(id).subscribe(data => {
      
      // this.pause = data;
      this.getRunningStrategy();
      this.getPausedStrategy();
    });

  }

  getPerformance(id) {
    console.log(id);
    this.strategyService.getPerformance(id).subscribe(data => {
      console.log(data);
      this.currentid = id;
      this.pnl = data.pnl;
      // this.roi = data.roi;
      // this.getRunningStrategy();
      // this.getPausedStrategy();
    });

  }

}
