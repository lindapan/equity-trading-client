import {Stock} from './stock';

export const STOCKS : Stock[] = [
    {  
        name: 'AAPL', 
        highPrice : 191.00,
        lowPrice : 189.56,
        currPrice : 190.16,
        open : 190.68,
        close : 190.16, 
        volume : 93600
    },

    {  
        name: 'GOOG', 
        highPrice : 1910.00,
        lowPrice : 1890.56,
        currPrice : 1900.16,
        open : 1900.68,
        close : 1900.16, 
        volume : 22222
    },

    {  
        name: 'WMT', 
        highPrice : 191.00,
        lowPrice : 189.56,
        currPrice : 190.16,
        open : 190.68,
        close : 190.16, 
        volume : 93600
    },

    {  
        name: 'C', 
        highPrice : 191.00,
        lowPrice : 189.56,
        currPrice : 190.16,
        open : 190.68,
        close : 190.16, 
        volume : 93600
    },

    {  
        name: 'INTC', 
        highPrice : 191.00,
        lowPrice : 189.56,
        currPrice : 190.16,
        open : 190.68,
        close : 190.16, 
        volume : 93600
    },

    {  
        name: 'TXN', 
        highPrice : 191.00,
        lowPrice : 189.56,
        currPrice : 190.16,
        open : 190.68,
        close : 190.16, 
        volume : 93600
    },

    {  
        name: 'MSFT', 
        highPrice : 191.00,
        lowPrice : 189.56,
        currPrice : 190.16,
        open : 190.68,
        close : 190.16, 
        volume : 93600
    },

    {  
        name: 'KO', 
        highPrice : 191.00,
        lowPrice : 189.56,
        currPrice : 190.16,
        open : 190.68,
        close : 190.16, 
        volume : 93600
    }
];